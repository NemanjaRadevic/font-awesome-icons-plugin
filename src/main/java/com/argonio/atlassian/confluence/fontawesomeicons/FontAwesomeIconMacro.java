package com.argonio.atlassian.confluence.fontawesomeicons;

import java.util.Map;

import com.atlassian.confluence.content.render.xhtml.ConversionContext;
import com.atlassian.confluence.macro.Macro;
import com.atlassian.confluence.macro.MacroExecutionException;
import com.atlassian.confluence.renderer.radeox.macros.MacroUtils;
import com.atlassian.confluence.util.velocity.VelocityUtils;
import com.atlassian.renderer.v2.components.HtmlEscaper;

/**
 *
 */
public class FontAwesomeIconMacro implements Macro {

    private static final String TEMPLATE = "/templates/font-awesome-icon-macro.vm";

    public FontAwesomeIconMacro() {

    }

    public String execute(Map<String, String> params, String s, ConversionContext conversionContext) throws MacroExecutionException {
            Map<String, Object> contextMap = MacroUtils.defaultVelocityContext();
            String iconClass = params.get("icon");
            String fontSize = params.get("size");
            String color = params.get("color");
            String monospace = params.get("monospace");

            contextMap.put("icon", HtmlEscaper.escapeAll(iconClass, false));
            contextMap.put("font_size", HtmlEscaper.escapeAll(fontSize, false));
            contextMap.put("color", HtmlEscaper.escapeAll(color, false));
            contextMap.put("monospace", monospace);

            return VelocityUtils.getRenderedTemplate(TEMPLATE, contextMap);
    }

    public BodyType getBodyType() {
        return BodyType.NONE;
    }

    public OutputType getOutputType() {
        return OutputType.INLINE;
    }
}
